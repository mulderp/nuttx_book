# NuttX Architecture

In order to use NuttX you will need to understand how NuttX is designed and organized.

![](/assets/nuttx_layers.svg)

> **TODO**: add a drawing of NuttX layers distinguishing between application layer, C/C++ library, architecture, board and device code

The main distinction to be made is in regards to **architecture** code, **board** code and **application** code.

In few words, in NuttX you will have various applications (such as system commands, the NuttX Shell and your own applications) that run in the from of **tasks**.

On the other hand, there will be *architecture* code which will support the particular micro-controller your application is built for and there will be *board* code which will support a particular instance of a specific microcontroller running in a particular hardware board (which may include on-board or external components connected to it).

In the middle between applications and the OS there will be a set of **drivers** supporting various kinds of devices, buses and other hardware abstractions. Applications will typically interact with these devices using standard I/O operating over character or block device files. 