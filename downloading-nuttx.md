# Downloading NuttX

NuttX sources are available in [BitBucket Git repositories](/bitbucket.org/nuttx/), where the latest version is always available. 

Every once in a while a new *release* is made by tagging the corresponding commit with a version number such as `nuttx-7.20` and with a corresponding ChangeLog entry update. In any case, working with the latest version is generally safe.

The NuttX main sources are available in the [**nuttx**](/bitbucket.org/nuttx/) repo. Another repository typically used in most cases is the [**apps**](https://bitbucket.org/nuttx/apps) repo.

It is recommended to organize these repositories in your machine to be sitting next to each other inside a common directory. Thus, downloading NuttX with the applications repo boils down to:

```bash
$ mkdir NuttX/ 
$ git clone https://bitbucket.org/nuttx/nuttx.git
$ git clone https://bitbucket.org/nuttx/apps.git
```
    
## How to organize your NuttX based project

For the typical case where you will be developing your project using Git, you can use submodules in order to track the relevant NuttX repositories while you maintain the changes to your own project. In this case you would do something like:

```bash
$ mkdir your_project
$ cd your_project
$ git init                # initialize your repository 
$ git submodule add nuttx https://bitbucket.org/nuttx/nuttx.git
$ git submodule add apps https://bitbucket.org/nuttx/nuttx.git
$ git commit -a -m 'my first commit'
```

There are of course other ways to organize your code (which do not require directly including NuttX in your repo), which will be covered in the following chapter.


    

 