# Contributing to NuttX

NuttX is developed in the main BitBucket repositories and is mainly maintained by Gregory Nutt.

Contributions are generally sent in the form of patches via the mailing list (see below). Pull requests on BitBucket can also be created.

In all cases please follow the [NuttX C coding standard](http://nuttx.org/doku.php?id=documentation:codingstandard).

Also, please note that there are certain design principles behind NuttX which your contribution should adhere to and is thus better to first ask for suggestions on proper integration in the mailing list.  

# Support

The main communication channel for interacting with other NuttX users and developers (and with Gregory Nutt himself) is via the [NuttX Yahoo Group](https://beta.groups.yahoo.com/neo/groups/nuttx).

Please ask questions here after searching for similar questions previously asked. Also be sure to look for information in the available documentation. Sometimes documentation will be lacking for certain aspect and the best way to learn something is to look at the code and read the comments. Also, since there are several boards, there are various usage and implementation examples of particular devices, components and applications.     