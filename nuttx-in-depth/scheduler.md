> **[info]** work in progress
> Is this information still up to date? Where can I get the information of the state transitions? 

# Scheduler

One important component of an operating system is a scheduler: the logic that controls when tasks or threads execute. Actually, more than that; the scheduler really determines what a task or a thread is! Most tiny operating systems, such as FreeRTOS are really not operating “systems” in the sense of providing a complete operating
environment. Rather these tiny operating systems consist really only of a scheduler. That is how important the scheduler is.

## State Transition Diagram
The state of a thread can then be easily represented with this simple state transition diagram:

> **[info]** to be provided

## Scheduling Policies

In order to be a real-time OS, an RTOS must support FIFO scheduling. That is, strict priority scheduling.
The thread with the highest priority runs. Period. The thread with the highest priority is always associated with the TCB at the head of the `g_readytorun` list.

NuttX supports one additional real-time scheduling policy. Specifically, it supports round-robin scheduling. In this case, NuttX supports timeslicing: If a task with round-robin scheduling policy is running, then when each timeslice elapses, it will give up the CPU to the next task that is at the same priority. Note that if there is only one task at this priority, round-robin and FIFO are the same. Moreover, FIFO tasks are never pre-empted in this
way.


1.2.1 Task Control Block (TCB)
In NuttX a thread is any controllable sequence of instruction execution that has its own stack. Each
task is represented by a data structure called a task control block or TCB. That data structure is defined
in the header file include/nuttx/sched.h.
1.2.2 Task Lists
These TCBs are retained in lists. The state of a task is indicated both by the task_state field of the
TCB and by a series of task lists. Although it is not always necessary, most of these lists are prioritized
so that common list handling logic can be used (only the g_readytorun, the g_pendingtasks,
and the g_waitingforsemaphore lists need to be prioritized).
All new tasks start in a non-running, uninitialized state:
volatile dq_queue_t g_inactivetasks;
This the list of all tasks that have been initialized, but not yet activated. NOTE: This is the only
list that is not prioritized.
When the task is initialized, it is moved to a read-to-run list. There are two lists representing ready-to-
run threads and several lists representing blocked threads. Here are the read-to-run threads:
volatile dq_queue_t g_readytorun;
This is the list of all tasks that are ready to run. The head of this list is the currently active task;
the tail of this list is always the idle task.
volatile dq_queue_t g_pendingtasks;
This is the list of all tasks that are ready-to-run, but cannot be placed in the g_readytorun
list because: (1) They are higher priority than the currently active task at the head of the
g_readytorun list, and (2) the currently active task has disabled pre-emption. These tasks will
stay in this holding list until pre-emption is again enabled (or the until the currently active task
voluntarily relinquishes the CPU).
Tasks in the g_readytorun list may become blocked. In this cased, their TCB will be moved to
one of the blocked lists. When the block task is ready-to-run, its TCB will be moved back to either the
g_readytorun to to the g_pendingtasks lists, depending up if pre-emption is disabled and
upon the priority of the tasks.
Here are the block task lists:
volatile dq_queue_t g_waitingforsemaphore;
This is the list of all tasks that are blocked waiting for a semaphore.
volatile dq_queue_t g_waitingforsignal;
This is the list of all tasks that are blocked waiting for a signal (only if signal support has not
been disabled)
volatile dq_queue_t g_waitingformqnotempty;
This is the list of all tasks that are blocked waiting for a message queue to become non-empty
(only if message queue support has not been disabled).
volatile dq_queue_t g_waitingformqnotfull;
This is the list of all tasks that are blocked waiting for a message queue to become non-full
(only if message queue support has not been disabled).
volatile dq_queue_t g_waitingforfill;
This is the list of all tasks that are blocking waiting for a page fill (only if on-demand paging is
selected).
(Reference nuttx/sched/os_internal.h).

1.2.5 Task IDs
Each task is represented not only by a TCB but also by a numeric task ID. Given a task ID, the RTOS
can find the TCB; given a TCB, the RTOS can find the task ID. So they are functionally equivalent.
Only the task ID, however, is exposed at the RTOS/application interfaces.