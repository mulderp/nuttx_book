# Tasks and Threads

## Task vs Process

When using NuttX, there's a distinction between *tasks* and *threads*. In few words, a *task* is similar to a typical Linux process. However, a key difference is that in NuttX, in contrast to other OS such as Linux or Windows, tasks do not have their own private space (and thus the term *process* is not used for NuttX). 

In order to implement private address spaces, the processor must support a memory management unit (MMU). The MMU is used to enforce the protected process environment. However, NuttX was designed to support the more resource constrained, lower-end, deeply embedded MCUs. Those MCUs seldom have an MMU and, as a consequence, can never support processes as are support by Windows and Linux. So NuttX does not support processes. 

For this reason, NuttX is a flat address OS. As such, it does not support processes each with its own separate address space. In other words, it only supports simple tasks and threads running within the same address space. 

However, a distinction can be made between the two.

## Task vs Thread

Tasks are threads which have a degree of independence, while threads share some resources. This applies, in particular, in the area of opened file descriptors and streams. When a task is started, it inherits (receives a duplicate) the open file descriptors of its parent task. Since these are duplicates, the child task can close them or manipulate them in any way without effecting the parent task. File-related operations (`open`, `close`, etc.) within a task will have no effect on other tasks. It is also possible to perform some level of redirection. 

If the NuttX system console is defined (as will be in most cases), the first three file descriptors inherted by a task will correspond to `stdin`, `stdout` and `stderr`.
Threads, on the other hand, will always share file descriptors with the parent thread (as it will with any other resource acquired in the task). In this case, file operations will have effect only all pthreads the were started from the same parent thread.

Threads, on the other hand, will always share file descriptors with the parent thread (as it will with any other resource acquired in the task). In this case, file operations will have effect only all pthreads the were started from the same parent thread.

In general, a task is a thread with an environment associated with it, which is private and unique to the task. This environment consists of a number of resources. Of interest in this discussion are the following (note that any of these task resources may be disabled in the NuttX
configuration to reduce the NuttX memory footprint).

 1. **Environment Variables**. This is the collection of variable assignments of the form: `VARIABLE=VALUE`
 2. **File Descriptors**. A file descriptor is a task specific number that represents an open resource (a file or a device driver, for example).
 3. **Sockets**. A socket descriptor is like a file descriptor, but the open resource in this case is a
network socket.
 4. **Streams**. Streams represent standard C buffered I/O. Streams wrap file descriptors or sockets a provide a new set of interface functions for dealing with the standard C I/O (like `fprintf()`, `fwrite()`, etc.).
 
In NuttX, a task is created using the interface `task_create()`.

## Programs (or applications) and tasks

A task can be considered an instance of a specific application. New tasks can be spawned in NuttX during run-time (either from code via a specific API or by running commands from the NuttX Shell).

However, another difference with respect to other standard Operating Systems is that applications may not necessarilly be a separate binary from the OS. In fact, the usual case will be to build NuttX and all its components (including applications) within a single binary. Tasks are then run mainly by function calls (performed by the OS) into entry points of these applications. These applications residing in the same NuttX image are known as **builtin applications**.

NuttX also supports the capability of executing **separate application binaries** residing in a filesystem. Different binary formats are supported, such as the standard ELF format and NuttX's own [NXFLAT](http://nuttx.org/Documentation/NuttXNxFlat.html) format. 





